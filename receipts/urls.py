from django.urls import path
from receipts.views import receipt_list, create_receipt, account_list,\
      expense_list, create_category, create_account

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/", expense_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),

]
# change added for push feature 15
