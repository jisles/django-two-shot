# Generated by Django 4.2.1 on 2023-06-02 01:15

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0002_rename_categroy_receipt_category"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
